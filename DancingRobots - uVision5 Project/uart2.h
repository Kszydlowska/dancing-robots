/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    uart2.h
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/

#ifndef uart2_h
#define uart2_h

#include "MKL46Z4.h"   /* Device header */

extern uint8_t data;
extern uint8_t test;

void UART2Initialize(void);					// initialization

uint8_t UART2CheckTXReady(void);		// check if buffer is empty -> next data may be transmitt			return 1 if ready, return 0 if not ready 
uint8_t UART2CheckRXReady(void);		// check if buffer is full  -> data are ready to read

void UART2TransmitData(char);				// transmit argument of function
char UART2ReceiveData(void);				// return received data

#endif
