/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    uart0.c
 *      Library for UART
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/

/**
 * @file uart0.c
 * @author KS
 * @date Jan 2016
 *
 */

#include "uart2.h"
#include "slcd.h" 
#include "motorDriver2.h"
#include "shiftbrite.h"

#define UART0_IRQ_NBR (IRQn_Type) 14
#define DIRECTION_MASK 128
#define SPEED_MASK 127
#define RGB_SPEED_MASK 15
#define RGB_MASK 240

volatile uint8_t counter = 0;
volatile uint8_t motorLeft = 0;
volatile uint8_t motorRight = 0;
volatile uint8_t RGB = 0;

uint8_t data;
uint8_t test;

void UART2Initialize() {
		SIM->SCGC5 |= SIM_SCGC5_PORTE_MASK; // connect clock to uart and port E
		SIM->SCGC4 |= SIM_SCGC4_UART2_MASK; 
	
		PORTE->PCR[16] = PORT_PCR_MUX(3); //RX & TX
		PORTE->PCR[17] = PORT_PCR_MUX(3);
	
		UART2->C2 &= ~UART_C2_TE_MASK; //disable transmitting and receiving
		UART2->C2 &= ~UART_C2_RE_MASK;
	
		UART2->BDH = 0; 		// set baud rate as 9600 bit/s
		UART2->BDL = 156;
	
		UART2->BDH &= ~UART_BDH_SBNS_MASK;    // set one stop bit

		UART2->C1 &= ~UART_C1_M_MASK;			//set 8-bit data
		UART2->C1 &= ~UART_C1_PE_MASK;			// disable parity cheecking
	
		UART2->C2 |= UART_C2_RIE_MASK | UART_C2_TIE_MASK; // enable interrupt requested when TDRE flag is 1, enable interrupt requested when RDRF flag is 1
	
		UART2->C2 |= UART_C2_TE_MASK | UART_C2_RE_MASK;  //enable transmitting and receiving
	
	
		NVIC_ClearPendingIRQ(UART2_IRQn);				// Clear NVIC any pending UART2 interrupts
		NVIC_EnableIRQ(UART2_IRQn);							// Enable NVIC interrupts source for UART2
		NVIC_SetPriority(UART2_IRQn, 3);				// Set priority for UART2 interrupts
}

uint8_t UART2CheckTXReady(void){
	if(UART0->S1 & UART_S1_TDRE_MASK)
		{
			return 1;
		}
		else
		{
			return 0;
		}
}

uint8_t UART2CheckRXReady(void){
	if(UART0->S1 & UART_S1_RDRF_MASK)
		{
			return 1;
		}
		else
		{
			return 0;
		}
}


void UART2TransmitData(char data){
	UART0->D = data;	
}

char UART2ReceiveData(void){
	return UART0->D;	
}

void UART2_IRQHandler(void) {		
	if (UART2->S1 & UART_S1_RDRF_MASK) {
			
		test = UART2->S1;
		data = UART2->D;
		slcdDisplay(data,3);
		test = UART2->S1;
		
				switch(counter)
			{
				case 0:
				{
					motorLeft = data;
					if ( motorLeft & DIRECTION_MASK){
						driveReverseLeftTrack( motorLeft & SPEED_MASK );
					}
					else{
						driveForwardLeftTrack( motorLeft & SPEED_MASK );
					}
					break;
				}

				case 1:
				{
					motorRight = data;
					if ( motorRight & DIRECTION_MASK){
						driveReverseRightTrack( motorRight & SPEED_MASK );
					}
					else{
						driveForwardRightTrack( motorRight & SPEED_MASK );
					}
					break;
				}
				
				case 2:
				{
					RGB = data;
					switch( RGB & RGB_MASK )
						{
						case 0:
							{
								NoCOLOR();
								break;
							}
						case 16:
							{
								RED();
								break;
							}
						case 32:
							{
								ORANGE();
								break;
							}
						case 48:
							{
								YELLOW();
								break;
							}
						case 64:
							{
								SPRING_GREEN();
								break;
							}
						case 80:
							{
								GREEN();
								break;
							}
						case 96:
							{
								TURQUISE();
								break;
							}
						case 112:
							{
								CYAN();
								break;
							}
						case 128:
							{
								OCEAN();
								break;
							}
						case 144:
							{
								BLUE();
								break;
							}
						case 160:
							{
								VIOLET();
								break;
							}
						case 176:
							{
								MAGENTA();
								break;
							}
						case 192:
							{
								RASPBERRY();
								break;
							}
						case 208:
							{
								Rainbow( RGB & RGB_SPEED_MASK );
								break;
							}
						}
				
				}
			}
			counter++;
			if (counter == 3){
				counter = 0;
			}
		
		test = UART2->S1;
	}
}

