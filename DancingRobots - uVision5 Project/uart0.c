/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    uart0.c
 *      Library for UART comunication with KL46Z
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/


/**
 * @file uart0.c
 * @author KS
 * @date Jan 2016
 * @brief Library offers functions for remote control robot zumo.
 * @note CLOCK SETUP must be set to 1
 */

#include "uart0.h"
#include "motorDriver2.h"
#include "shiftbrite.h"

#define UART0_IRQ_NBR (IRQn_Type) 12

#define DIRECTION_MASK 128
#define SPEED_MASK 127
#define RGB_SPEED_MASK 15
#define RGB_MASK 240

volatile uint8_t counter = 0;
volatile uint8_t motorLeft = 0;
volatile uint8_t motorRight = 0;
volatile uint8_t RGB = 0;

static uint8_t data;
static uint8_t test;


/**@brief Initialization function.
 *
 * Use this function initialize UART0 module.
 * @note Settings:
 - Master clock: 8MHz - Asynch Module Clock
 - Over Sampling Ratio: 31
 - One stop bit
 - No hardware parity generation or checking
 - Receiver and transmitter use 8-bit data characters
 - Interrupts for the transmitter and receiver are active.
 @param baud_rate - recommended value: 9600
 */
void UART0Initialize(uint32_t baud_rate){
	uint16_t BDH_temp;
  uint16_t SBR;
	
	SIM->SCGC4 |= SIM_SCGC4_UART0_MASK;
	SIM->SCGC5 |= SIM_SCGC5_PORTA_MASK;
	
	PORTA->PCR[1] |= PORT_PCR_MUX(2);
	PORTA->PCR[2] |= PORT_PCR_MUX(2);

	SIM->SOPT2 |= SIM_SOPT2_UART0SRC(2);
	
	UART0->C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK);
	
	UART0->C4 |= UART0_C4_OSR(31);
	
	SBR = (uint16_t)((8000000)/(baud_rate * 32)); 
	BDH_temp = UART0->BDH & ~(UART_BDH_SBR(0x1F));  
      
  UART0->BDH = BDH_temp |  UART_BDH_SBR(((SBR & 0x1F00) >> 8));
  UART0->BDL = (char)(SBR & UART_BDL_SBR_MASK);  
	//UART0->BDH &= ~UART_BDH_SBR_MASK;			
	//UART0->BDL = 26;
	
	UART0->BDH &= ~UART_BDH_SBNS_MASK;
	
	UART0->C1 &= ~UART_C1_M_MASK;
	UART0->C1 &= ~UART_C1_PE_MASK;
	
	UART0->C2 |= UART_C2_TIE_MASK;
	UART0->C2 |= UART_C2_RIE_MASK;
	
	UART0->C2 |= (UART_C2_TE_MASK | UART_C2_RE_MASK);
	
	NVIC_ClearPendingIRQ(UART0_IRQ_NBR);
	NVIC_EnableIRQ(UART0_IRQ_NBR);	
}

/**@brief Check transmitter ready.
 *	Function checking if the transmitter is ready to transmit.
 * @return Returns 1 when the transmitter empty, otherwise it returns 0.
 */

bool UART0CheckTXReady(void){
	if(UART0->S1 & UART_S1_TDRE_MASK){
			return 1;
		}
		else{
			return 0;
		}
}

/**@brief Check receiver ready.
 *	Function checking if the receiver is ready.
 * @return Returns 1 when the receiver is full, otherwise it returns 0.
 */
bool UART0CheckRXReady(void){
	if(UART0->S1 & UART_S1_RDRF_MASK){
			return 1;
		}
		else{
			return 0;
		}
}
/**@brief Transmit Data.
 *	Sending data (type uint8_t).
*/
void UART0TransmitData(uint8_t data){
	UART0->D = data;	
}
/**@brief Receive Data.
 *	Receiving data (type uint8_t).
*/
uint8_t UART0ReceiveData(void){
	return UART0->D;	
}
/**@brief IRQHandler.
 *	Interrupt used to translate received data to motors and RGB led commands.
*/
void UART0_IRQHandler(void){
	
if (UART0->S1 & UART_S1_RDRF_MASK) {
		
		test = UART0->S1;
		data = UART0->D;
		
		//UART0->D = data;
		test = UART0->S1;
	
			switch(counter)
			{
				case 0:
				{
					motorLeft = data;
					if ( motorLeft & DIRECTION_MASK){
						driveReverseLeftTrack( motorLeft & SPEED_MASK );
					}
					else{
						driveForwardLeftTrack( motorLeft & SPEED_MASK );
					}
					break;
				}

				case 1:
				{
					motorRight = data;
					if ( motorRight & DIRECTION_MASK){
						driveReverseRightTrack( motorRight & SPEED_MASK );
					}
					else{
						driveForwardRightTrack( motorRight & SPEED_MASK );
					}
					break;
				}
				
				case 2:
				{
					RGB = data;
					switch( RGB & RGB_MASK )
						{
						case 0:
							{
								break;
							}
						case 16:
							{
								RED();
								break;
							}
						case 32:
							{
								ORANGE();
								break;
							}
						case 48:
							{
								YELLOW();
								break;
							}
						case 64:
							{
								SPRING_GREEN();
								break;
							}
						case 80:
							{
								GREEN();
								break;
							}
						case 96:
							{
								TURQUISE();
								break;
							}
						case 112:
							{
								CYAN();
								break;
							}
						case 128:
							{
								OCEAN();
								break;
							}
						case 144:
							{
								BLUE();
								break;
							}
						case 160:
							{
								VIOLET();
								break;
							}
						case 176:
							{
								MAGENTA();
								break;
							}
						case 192:
							{
								RASPBERRY();
								break;
							}
						case 208:
							{
								Rainbow( RGB & RGB_SPEED_MASK );
								break;
							}
						}
				
				}
			}
			counter++;
			if (counter == 3){
				counter = 0;
			}

		}
		UART0->S1;
}



