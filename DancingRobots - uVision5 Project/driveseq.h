/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:  driveseq.h
 *      Library of sequences of movements of the robot zumo
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/
 /**
 * @file driveseq.h
 * @author KS
 * @date Jan 2016
 * @brief Library of sequences of movements of the robot zumo
 *
 */
#ifndef driveseq_h
#define driveseq_h

#include "MKL46Z4.h"

void star(void);
void square(void);
void regular12(void);
void circle(void);
void wolo_lolo(void);

#endif
