/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    uart0.h
 *      Header file
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/
 
#ifndef uart0_h
#define uart0_h

#include "MKL46Z4.h" 

void UART0Initialize(uint32_t baud_rate);

bool UART0CheckTXReady(void);
bool UART0CheckRXReady(void);

void UART0TransmitData(uint8_t);
uint8_t UART0ReceiveData(void);

#endif
