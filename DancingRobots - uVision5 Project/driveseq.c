/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:  driveseq.h
 *      Library of sequences of movements of the robot zumo
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/
 /**
 * @file driveseq.h
 * @author KS
 * @date Jan 2016
 * @brief Library of sequences of movements of the robot zumo
 *
 */

#include "driveseq.h"
#include "shiftbrite.h"
#include "motorDriver2.h"

void star(void){
	Rainbow(1);
	delay_mc(800);
	
	RED();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(600);
	
	TURQUISE();
	driveReverseRightTrack( 40 );	
	driveReverseLeftTrack( 40 );
	delay_mc(600);
	
	MAGENTA();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(300);
	
	SPRING_GREEN();
	driveForwardLeftTrack(25);
	driveReverseRightTrack(25);
	delay_mc(200);
	
	BLUE();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(300);
	
	ORANGE();
	driveReverseRightTrack( 40 );	
	driveReverseLeftTrack( 40 );
	delay_mc(600);
	
	CYAN();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(300);
	
	RASPBERRY();
	driveForwardLeftTrack(25);
	driveReverseRightTrack(25);
	delay_mc(200);
	
	GREEN();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(300);
	
	VIOLET();
	driveReverseRightTrack( 40 );	
	driveReverseLeftTrack( 40 );
	delay_mc(600);
	
	YELLOW();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(300);
	
	OCEAN();
	driveForwardLeftTrack(25);
	driveReverseRightTrack(25);
	delay_mc(200);
	
	driveForwardLeftTrack(0);
	driveReverseRightTrack(0);
	Rainbow(1);
	
}

void square(void){
	
	Rainbow(1);
	delay_mc(500);
		
	RED();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(400);

	driveForwardLeftTrack(44);
	driveReverseRightTrack(44);
	delay_mc(100);
	driveForwardLeftTrack(0);
	driveReverseRightTrack(0);
	
	GREEN();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(400);

	driveForwardLeftTrack(44);
	driveReverseRightTrack(44);
	delay_mc(100);
	driveForwardLeftTrack(0);
	driveReverseRightTrack(0);
	
	YELLOW();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(400);

	driveForwardLeftTrack(44);
	driveReverseRightTrack(44);
	delay_mc(100);
	driveForwardLeftTrack(0);
	driveReverseRightTrack(0);
	
	BLUE();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(400);

	driveForwardLeftTrack(44);
	driveReverseRightTrack(44);
	delay_mc(100);
	driveForwardLeftTrack(0);
	driveReverseRightTrack(0);
	delay_mc(1000);
}

void regular12(void){
	
	RED();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);

	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);

	ORANGE();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	YELLOW();driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	SPRING_GREEN();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	GREEN();driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	TURQUISE();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	CYAN();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	OCEAN();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	BLUE();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	VIOLET();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	MAGENTA();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	RASPBERRY();
	driveForwardRightTrack( 40 );	
	driveForwardLeftTrack( 40 );
	delay_mc(100);
	
	driveReverseLeftTrack(35);
	driveForwardRightTrack(35);
	delay_mc(100);
	
	driveForwardRightTrack( 0 );	
	driveForwardLeftTrack( 0 );
	delay_mc(1000);
}

void circle(void){
	TURQUISE();
	
	driveReverseLeftTrack( 30 );
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseLeftTrack( 0 );
	driveReverseRightTrack( 70 );
	delay_mc(100);
	driveReverseLeftTrack( 30 );
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseLeftTrack( 0 );
	driveReverseRightTrack( 70 );
	delay_mc(100);
	driveReverseLeftTrack( 30 );
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseLeftTrack( 0 );
	driveReverseRightTrack( 70 );
	delay_mc(100);
	driveReverseLeftTrack( 30 );
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseLeftTrack( 0 );
	driveReverseRightTrack( 70 );
	delay_mc(100);
	driveReverseLeftTrack( 30 );
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseLeftTrack( 0 );
	driveReverseRightTrack( 70 );
	delay_mc(100);
	driveReverseLeftTrack( 30 );
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseLeftTrack( 0 );
	driveReverseRightTrack( 70 );
	delay_mc(100);
	
	driveReverseLeftTrack( 0 );	
	driveReverseRightTrack( 0 );
	delay_mc(1000);
	
}

void wolo_lolo(void){
	RED();
	driveReverseLeftTrack( 100 );	
	driveReverseRightTrack( 100 );
	delay_mc(100);
	
	BLUE();
	driveForwardLeftTrack( 50 );	
	driveReverseRightTrack( 50 );
	delay_mc(100);
	
	GREEN();
	driveForwardLeftTrack( 80 );	
	driveForwardRightTrack( 80 );
	delay_mc(200);
	
	BLUE();
	driveReverseLeftTrack( 50 );	
	driveForwardRightTrack( 50 );
	delay_mc(100);
	
	GREEN();
	driveForwardLeftTrack( 50 );	
	driveForwardRightTrack( 50 );
	delay_mc(200);
	
	BLUE();
	driveForwardLeftTrack( 50 );	
	driveReverseRightTrack( 50 );
	delay_mc(300);
	
	GREEN();
	driveForwardLeftTrack( 50 );	
	driveForwardRightTrack( 50 );
	delay_mc(200);
	
	RED();
	driveReverseLeftTrack( 30 );	
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseRightTrack( 30 );
	driveReverseLeftTrack( 0 );	
	delay_mc(100);
	
	driveReverseLeftTrack( 30 );	
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseRightTrack( 30 );
	driveReverseLeftTrack( 0 );
	delay_mc(100);
	
	driveReverseLeftTrack( 30 );	
	driveReverseRightTrack( 30 );
	delay_mc(200);
	driveReverseRightTrack( 30 );
	driveReverseLeftTrack( 0 );
	delay_mc(100);
	
	driveReverseLeftTrack( 0 );	
	driveReverseRightTrack( 0 );
	delay_mc(1000);
}
