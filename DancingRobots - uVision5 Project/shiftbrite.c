/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    shiftbrite.c
 *      Library for KL46Z and Allegro A6281 LED driver module
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/

/**
 * @file shiftbrite.c
 * @author KS
 * @date Jan 2016
 * @brief Library for KL46Z and Allegro A6281 LED driver module.
 *
 */


#include "shiftbrite.h"

const uint32_t data_mask   = 1UL<<DATA; 	 // Data is Port B bit 0
const uint32_t latch_mask  = 1UL<<LATCH;   // Latch is Port B bit 1
const uint32_t enable_mask = 1UL<<ENABLE;  // Enable is Port B bit 2
const uint32_t clock_mask  = 1UL<<CLOCK;   // Clock is Port B bit 3

/**@brief Initialization function.
 *
 * Use this function to be able to use the other functions of this library.
 */

void shiftBriteInitialize(void){
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK;                   // Enable Clock to Port B 
	PORTB->PCR[DATA] = PORT_PCR_MUX(1);                   // Pin PTB0 is GPIO
	PORTB->PCR[LATCH] = PORT_PCR_MUX(1);                  // Pin PTB1 is GPIO
	PORTB->PCR[ENABLE] = PORT_PCR_MUX(1);                 // Pin PTB2 is GPIO
	PORTB->PCR[CLOCK] = PORT_PCR_MUX(1);                  // Pin PTB3 is GPIO
		
	FPTB->PDDR = (data_mask | latch_mask | enable_mask | clock_mask);         // enable DataPin, LatchPin, EnablePin and ClockPin as Outputs
			
	// Set the Enable and Latch outputs low to enable the ShiftBrites.
	FPTB->PCOR = enable_mask;
	FPTB->PCOR = latch_mask;
}

/**@brief colorPacket sets color brightnesses and returns a ShiftBritePacket
 *
 * This function sets color brightnesses and returns a ShiftBritePacket (ready to send to module) red, green, and blue are brightness values from 0 to 127. 0 is off, and 127 is brightest (33% - 100%)
 * @param value "shiftbrite_packet.value = 0x00000000"  Make a packet and initialize all of the bits to zero.
 * @param red brightness value from 0 to 127. 0 is off, and 127 is brightest.
 * @param green brightness value from 0 to 127. 0 is off, and 127 is brightest.
 * @param blue brightness value from 0 to 127. 0 is off, and 127 is brightest.
 * @param clockMode Clock Mode selection. Options: 00, 01, 10, 11 (default: 00, if you do not need, do not change)
 * @see See page 7 of A6281 datasheet for more information about clockMode.
 * @param command command = 1 is used to send packet
 * @return Function returns shiftbrite_packet - 32 bits ready to send to module.
 * @note Values out of range will cause incorrect acction
*/

ShiftBritePacket colorPacket(unsigned int red, unsigned int green, unsigned int blue, unsigned char clockMode){
	
//Make a packet and initialize all of the bits to zero.
	ShiftBritePacket shiftbrite_packet;
	shiftbrite_packet.value = 0x00000000;
	shiftbrite_packet.red   = red; 
	shiftbrite_packet.green = green;
	shiftbrite_packet.blue  = blue;
	shiftbrite_packet.clockMode = clockMode;
	shiftbrite_packet.command = 1;
	return shiftbrite_packet;
}
/**@brief Toggle LatchPin
 * This function will once toggle Latch (we need to change LI Pin high to send 32-bits packet (when shift register is full)
 * The LI pin must be brought low before the rising edge of the next clock pulse, to avoid latching erroneous data
 * @note This function is in sendPacket - you do not need to call her again.
*/


void latch(void){
	// Set Latch high
	FPTB->PSOR = latch_mask;
	FPTB->PSOR = enable_mask;
	// Set Latch low
	FPTB->PCOR = enable_mask;
	FPTB->PCOR = latch_mask;
}

/**@brief serial (FIFO) sending a data packet from MSB. 
  This function sends data to module.
 * @param shiftbrite_packet - 32 bits packet returned by colorPacket
*/

void sendPacket(ShiftBritePacket shiftbrite_packet){		
	for(i = 0; i < 32; i++)
	{
		//Set the appropriate Data In value according to the packet.
		if ((shiftbrite_packet.value >> (31 - i)) & 1){
			FPTB->PSOR = data_mask;
		}
		else{
			FPTB->PCOR = data_mask;
		}
		//Toggle the clock bit twice.
		FPTB->PTOR = clock_mask;
		FPTB->PTOR = clock_mask;
	}
	latch();
}

/** @brief This function makes delay of 'delay' * 10000 clock cycles
 * @param delay type uint32_t
*/

void delay_mc(uint32_t delay){
	for(x = 0; x < delay; x++){
		for(i = 0; i < 10000; i++){};
	}
}

/** @brief Function making the RGB LED will light in rainbow colors.
 * Function making the RGB LED will light sequentially in all colors of the rainbow. It starts with the color red, passing through yellow, green, blue and purple. The colors go round and round.
 * @param speed Parameter which controls speed of color changes. 1 - the slowest, the higher value the faster.
 * @note Values over 10 causes no effective changes (too slow).
*/

void Rainbow(uint8_t speed){
	
	// 1) RED
	for (shiftbrite_packet.red = 126, shiftbrite_packet.green = 0, shiftbrite_packet.blue = 0 ; shiftbrite_packet.green < 62; shiftbrite_packet.green++,shiftbrite_packet.green++){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 2) ORANGE
	for (shiftbrite_packet.red = 126, shiftbrite_packet.green = 62, shiftbrite_packet.blue = 0 ; shiftbrite_packet.green < 126; shiftbrite_packet.green++,shiftbrite_packet.green++){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 3) YELLOW
		for (shiftbrite_packet.red = 126, shiftbrite_packet.green = 126, shiftbrite_packet.blue = 0 ; shiftbrite_packet.red > 62; shiftbrite_packet.red--,shiftbrite_packet.red--){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 4) SPRING GREEN
	for (shiftbrite_packet.red = 62, shiftbrite_packet.green = 126, shiftbrite_packet.blue = 0 ; shiftbrite_packet.red > 0; shiftbrite_packet.red--,shiftbrite_packet.red--){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 5) GREEN
	for (shiftbrite_packet.red = 0, shiftbrite_packet.green = 126, shiftbrite_packet.blue = 0 ; shiftbrite_packet.blue < 62; shiftbrite_packet.blue++,shiftbrite_packet.blue++){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 6) TURQUISE
	for (shiftbrite_packet.red = 0, shiftbrite_packet.green = 126, shiftbrite_packet.blue = 62 ; shiftbrite_packet.blue < 126; shiftbrite_packet.blue++,shiftbrite_packet.blue++){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 7) CYAN
	for (shiftbrite_packet.red = 0, shiftbrite_packet.green = 126, shiftbrite_packet.blue = 126 ; shiftbrite_packet.green > 62; shiftbrite_packet.green--,shiftbrite_packet.green--){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 8) OCEAN
	for (shiftbrite_packet.red = 0, shiftbrite_packet.green = 62, shiftbrite_packet.blue = 126 ; shiftbrite_packet.green > 0; shiftbrite_packet.green--,shiftbrite_packet.green--){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 9) BLUE
	for (shiftbrite_packet.red = 0, shiftbrite_packet.green = 0, shiftbrite_packet.blue = 126 ; shiftbrite_packet.red < 62; shiftbrite_packet.red++,shiftbrite_packet.red++){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 10) VIOLET
	for (shiftbrite_packet.red = 62, shiftbrite_packet.green = 0, shiftbrite_packet.blue = 126 ; shiftbrite_packet.red < 126; shiftbrite_packet.red++,shiftbrite_packet.red++){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 11) MAGENTA
	for (shiftbrite_packet.red = 126, shiftbrite_packet.green = 0, shiftbrite_packet.blue = 126 ; shiftbrite_packet.blue > 62; shiftbrite_packet.blue--,shiftbrite_packet.blue--){
		sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
	// 12) RASPBERRY
	for (shiftbrite_packet.red = 126, shiftbrite_packet.green = 0, shiftbrite_packet.blue = 62 ; shiftbrite_packet.blue > 0; shiftbrite_packet.blue--,shiftbrite_packet.blue--){
	sendPacket(shiftbrite_packet);
		delay_mc(speed);
	}
}
/** @brief Function making the RGB LED lights in red color. 
*/

void RED(void){
	shiftbrite_packet.red = 126;
	shiftbrite_packet.green = 0;
	shiftbrite_packet.blue = 0 ;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in orange color. 
*/

void ORANGE(void){
	shiftbrite_packet.red = 126;
	shiftbrite_packet.green = 62;
	shiftbrite_packet.blue = 0;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in yellow color. 
*/

void YELLOW(void){
	shiftbrite_packet.red = 126;
	shiftbrite_packet.green = 126;	
	shiftbrite_packet.blue = 0;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in spring green color. 
*/

void SPRING_GREEN(void){
	shiftbrite_packet.red = 62;
	shiftbrite_packet.green = 126;
	shiftbrite_packet.blue = 0;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in green color. 
*/

void GREEN(void){
	shiftbrite_packet.red = 0;
	shiftbrite_packet.green = 126;
	shiftbrite_packet.blue = 0;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in turquise color. 
*/

void TURQUISE(void){
	shiftbrite_packet.red = 0;
	shiftbrite_packet.green = 126;
	shiftbrite_packet.blue = 62;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in cyan color. 
*/

void CYAN(void){
	shiftbrite_packet.red = 0;
	shiftbrite_packet.green = 126;
	shiftbrite_packet.blue = 126;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in ocean color. 
*/

void OCEAN(void){
	shiftbrite_packet.red = 0;
	shiftbrite_packet.green = 62;
	shiftbrite_packet.blue = 126;
	sendPacket(shiftbrite_packet);		
}


/** @brief Function making the RGB LED lights in blue color. 
*/

void BLUE(void){
	shiftbrite_packet.red = 0;
	shiftbrite_packet.green = 0;
	shiftbrite_packet.blue = 126;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in violet color. 
*/

void VIOLET(void){
	shiftbrite_packet.red = 62;
	shiftbrite_packet.green = 0;
	shiftbrite_packet.blue = 126;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in magenta color. 
*/

void MAGENTA(void){
	shiftbrite_packet.red = 126;
	shiftbrite_packet.green = 0;
	shiftbrite_packet.blue = 126;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED lights in raspberry color. 
*/

void RASPBERRY(void){
	shiftbrite_packet.red = 126;
	shiftbrite_packet.green = 0;
	shiftbrite_packet.blue = 62;
	sendPacket(shiftbrite_packet);
}

/** @brief Function making the RGB LED no color. 
*/

void NoCOLOR(void){
	shiftbrite_packet.red = 0;
	shiftbrite_packet.green = 0;
	shiftbrite_packet.blue = 0;
	sendPacket(shiftbrite_packet);
}

