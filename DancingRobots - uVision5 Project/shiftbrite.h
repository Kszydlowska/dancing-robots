/*----------------------------------------------------------------------------
 *      
 *----------------------------------------------------------------------------
 *      Name:    shiftbrite.h
 *      Library for KL46Z and Allegro A6281 LED driver module
 *----------------------------------------------------------------------------
 *      
 *---------------------------------------------------------------------------*/
 /**
 * @file shiftbrite.c
 * @author KS
 * @date Jan 2016
 * @brief Library for KL46Z and Allegro A6281 LED driver module.
 *
 */
#ifndef shiftbrite_h
#define shiftbrite_h

#include "MKL46Z4.h"

/**
 * @brief These pragmas enable and disable support for anonymous structures and unions.
 */

#pragma anon_unions

/**
 * @brief Definition of used pins.
 */
 
#define DATA 0
#define LATCH 1
#define ENABLE 2
#define CLOCK 3


static uint32_t i, x;

void shiftBriteInitialize(void);

/**
 * @brief This union represents 32-bit data packet.
 */
 
typedef union ShiftBritePacket{ 
	
    uint32_t value;
    struct
    {
        unsigned green:7;			// first 7 bits control brightness of green led
				unsigned clockMode:2; // (7&8 bits) clock mode selection: 00 - 800kHz, 10 - 400kHz, 01 - external (cuont on rising edge of Clock signal), 11 - 200kHz
        unsigned :1; 					// don't care
        unsigned red:7;			  // 7 bits (10-16) control brightness of red led
        unsigned :3; 					// dont't care
        unsigned blue:7;		  // 7 bits (20-26) control brightness of blue led
				unsigned :3;					// dont't care.
				unsigned command:1;   // Selects which word is written to: Dot Correction/Clock Mode selection or PWM counter -> 0
    };
} ShiftBritePacket;
static ShiftBritePacket shiftbrite_packet;

ShiftBritePacket colorPacket(unsigned int red, unsigned int green, unsigned int blue, unsigned char clockMode);
void sendPacket(ShiftBritePacket shiftbrite_packet);
void latch(void);
void delay_mc(uint32_t delay);
	
void Rainbow(uint8_t speed);
void RED(void);
void ORANGE(void);
void YELLOW(void);
void SPRING_GREEN(void);
void GREEN(void);
void TURQUISE(void);
void CYAN(void);
void OCEAN(void);
void BLUE(void);
void VIOLET(void);
void MAGENTA(void);
void RASPBERRY(void);
void NoCOLOR(void);

#endif
