var searchData=
[
  ['rainbow',['Rainbow',['../shiftbrite_8c.html#a563b4a3a77dcd1e02e19d2977b6156ce',1,'Rainbow(uint8_t speed):&#160;shiftbrite.c'],['../shiftbrite_8h.html#a563b4a3a77dcd1e02e19d2977b6156ce',1,'Rainbow(uint8_t speed):&#160;shiftbrite.c']]],
  ['raspberry',['RASPBERRY',['../shiftbrite_8c.html#a362e3386928533a7021c2ba695138015',1,'RASPBERRY(void):&#160;shiftbrite.c'],['../shiftbrite_8h.html#a362e3386928533a7021c2ba695138015',1,'RASPBERRY(void):&#160;shiftbrite.c']]],
  ['red',['red',['../union_shift_brite_packet.html#a95997d4fcc617cfa418ef994aab8691b',1,'ShiftBritePacket::red()'],['../shiftbrite_8c.html#a4307e41272a15c7be08062c2d2a9817e',1,'RED(void):&#160;shiftbrite.c'],['../shiftbrite_8h.html#a4307e41272a15c7be08062c2d2a9817e',1,'RED(void):&#160;shiftbrite.c']]],
  ['rgb',['RGB',['../uart0_8c.html#a18d0587ad9a81ddf54fde06df053bb45',1,'uart0.c']]],
  ['rgb_5fmask',['RGB_MASK',['../uart0_8c.html#aba57c706b379c68e87a652764d8c93b0',1,'uart0.c']]],
  ['rgb_5fspeed_5fmask',['RGB_SPEED_MASK',['../uart0_8c.html#adf1afc8f4e67119aa9a29469205882ee',1,'uart0.c']]]
];
