var searchData=
[
  ['clock',['CLOCK',['../shiftbrite_8h.html#a3be7ef61d339af381862a81d4b363efb',1,'shiftbrite.h']]],
  ['clock_5fmask',['clock_mask',['../shiftbrite_8c.html#ad1056b1d8dc72aed59a8466f8a0de6b8',1,'shiftbrite.c']]],
  ['clockmode',['clockMode',['../union_shift_brite_packet.html#a82b20712588d66ce1e89c0df5d392446',1,'ShiftBritePacket']]],
  ['colorpacket',['colorPacket',['../shiftbrite_8c.html#ac5a776a479c4d8027247d4e2bbae50a8',1,'colorPacket(unsigned int red, unsigned int green, unsigned int blue, unsigned char clockMode):&#160;shiftbrite.c'],['../shiftbrite_8h.html#ac5a776a479c4d8027247d4e2bbae50a8',1,'colorPacket(unsigned int red, unsigned int green, unsigned int blue, unsigned char clockMode):&#160;shiftbrite.c']]],
  ['command',['command',['../union_shift_brite_packet.html#a289e744d95a59c08191a9232ff49052f',1,'ShiftBritePacket']]],
  ['counter',['counter',['../uart0_8c.html#a5e78baefc73f416a687e0b402aed1eb2',1,'uart0.c']]],
  ['cyan',['CYAN',['../shiftbrite_8c.html#adc5114052093fed1bd4a454bbc388a48',1,'CYAN(void):&#160;shiftbrite.c'],['../shiftbrite_8h.html#adc5114052093fed1bd4a454bbc388a48',1,'CYAN(void):&#160;shiftbrite.c']]]
];
